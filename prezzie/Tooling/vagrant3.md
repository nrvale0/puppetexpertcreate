<!SLIDE>
# Vagrant

Disposable repeatable VMs with:

* VirtualBox
* VMWare
* LXC
* Docker
* etc
