<!SLIDE center>
# Git

For better or worse (Hg lovers), Git dominates in the Puppet community.

* GitHub : enough said
* BitBucket : <http://bitbucket.org>
  * free private repos
* 'The Git Book' : <http://git-scm.com/book>
* 'Learn Git Branching' : <http://pcottle.github.io/learnGitBranching/>
