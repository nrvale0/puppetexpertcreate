<!SLIDE>
# Easy
* "What is Puppet?" with basic examples
* clever things others have done with Puppet
* How to DevOps-ify your organization with Puppet as a core tool
