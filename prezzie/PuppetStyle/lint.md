<!SLIDE center>
# Style Guide Check

<pre class='sh_shell forge'>
$ sudo gem install puppet-lint && puppet-lint --with-filename ./mymodule
</pre>

* <http://puppet-lint.com>
* very basic sanity check ( brackets, commas, quotes, etc )
* doesn't check for valid attributes for the type
