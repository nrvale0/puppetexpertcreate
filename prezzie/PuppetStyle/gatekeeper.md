<!SLIDE center>
# Puppet Style ProTip

Implement logic which rejects submission of Puppet code
code to your central git/SVN/etc repositories unless the code passes both
'puppet parser' and puppet-lint runs.
