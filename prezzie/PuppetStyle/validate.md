<!SLIDE center>
# Validate Your Code

<pre class='sh_shell forge'>
$ ( cd mymodule && puppet parser validate `find ./ -name '*.pp'` )
</pre>

* very basic sanity check ( brackets, commas, etc)
* doesn't check for valid attributes for the type
