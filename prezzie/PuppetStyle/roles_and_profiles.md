<!SLIDE center>

# Roles and Profiles

* higher and higher levels of abstraction for Puppet code
* first publicly proposed by Craig Dunn in:
  * <http://www.craigdunn.org/2012/05/239/>
* now taught as part of Puppet Labs training

* roles composed of profiles
* profiles composed of component modules (puppetlabs/apache)

* Let's see some code...
