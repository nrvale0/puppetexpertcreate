<!SLIDE center>
# Hiera

* source Puppet top-scopes and class parameters from multiple backends
* YAML backend is most popular. Also JSON.
* other backends allow non-coders to fiddle with data fed to pre-written modules
* CouchDB, MySQL, LDAP, etc - lots of Hiera backends in the community
