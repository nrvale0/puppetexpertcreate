<!SLIDE center>
# Online Puppet Lint

puppet-lint As A Service?

* <http://www.puppetlinter.com>

And did you know it has a GitHub Post-Receive Hook?

* <http://www.puppetlinter.com/github>

